package noise;

import javax.swing.*;
import java.awt.*;

public class Main {
    private static class Frame extends JFrame {
        Frame() {
            setPreferredSize(new Dimension(1024, 1024));
            setVisible(true);
            pack();
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setLayout(new GridLayout(1, 1));
        }
    }

    private static class View extends Canvas {
        private final double _f;
        private final BSplineNoise _noise = new BSplineNoise();

        View(double f) {
            _f = 1. / f;
            this.setPreferredSize(new Dimension(1000, 1000));
            this.setVisible(true);
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            for (int x = 0; x < getWidth(); ++x) {
                for (int y = 0; y < getHeight(); ++y) {
                    double v = y < getHeight() / 2 ?
                            _noise.quadratic(_f * x, _f * y) :
                            _noise.cubic(_f * x, _f * y);

                    v = (v + 1.) * 0.5;
                    float fv = (float) v;
                    g.setColor(new Color(fv, fv, fv, 1));
                    g.drawLine(x, y, x, y);
                }
            }
            getGraphics().setColor(new Color(1, 1, 1, 1));
        }
    }

    public static void main(String[] args) {
        Frame frame = new Frame();
        frame.add(new View(30));
    }
}
